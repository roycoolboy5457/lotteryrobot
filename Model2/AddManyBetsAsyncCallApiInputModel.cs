﻿using Model2.LotteryProjectModel;
using Model2.LotteryProjectModel.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model2
{
    public class AddManyBetsAsyncCallApiInputModel
    {
        public UserModel userInfo { get; set; }
        public RequestHelper requestHelper { get; set; }
        public string currentRoundCode { get; set; }
        public LotteryTypeEnum lotteryTypeEnum { get; set; }
        public long betCount { get; set; }
    }
}
