﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model2.LotteryProjectModel
{
    /// <summary>
    /// 分頁。
    /// </summary>
    public class Pagination
    {
        public Pagination()
        {
            OrderBy = "";
            ASC = -1;
            //SortableColumn = new List<string>();
        }

        /// <summary>
        /// 頁碼。
        /// </summary>
        public int? PageIndex { get; set; }

        /// <summary>
        /// 每頁筆數。
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// 總筆數。
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 總頁數。
        /// </summary>
        public int? PageCount
        {
            get
            {
                if (PageIndex == null || PageSize == null ||
                    PageIndex <= 0 || PageSize <= 0 || TotalCount <= 0)
                {
                    return 0;
                }

                return (TotalCount + PageSize - 1) / PageSize;
            }
        }

        /// <summary>
        /// 略過筆數
        /// </summary>     
        public int PageSkip
        {
            get
            {
                return (PageIndex - 1) * PageSize ?? 0;
            }
        }

        /// <summary>
        /// 排序欄位。
        /// </summary>   
        public string OrderBy { get; set; }

        /// <summary>
        /// 排序。(升冪:1，降冪:0)
        /// </summary>
        public int ASC { get; set; }

        /// <summary>
        /// 可排序欄位。
        /// </summary>   
        public List<string> SortableColumn { get; set; }

    }
}
