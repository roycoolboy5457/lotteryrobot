﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Model2.LotteryProjectModel
{
    public class MD5Helper
    {
        #region Constructors

        /// <summary>
        /// MD5 演算法。
        /// </summary>
        public MD5Helper()
        {

        }

        #endregion


        #region Public Methods

        /// <summary>
        /// 編碼。
        /// </summary>
        /// <param name="value">要編碼的內容。</param>
        /// <returns></returns>
        public string Encrypt(byte[] value)
        {
            string output = string.Empty;
            // HashAlgorithm algorithm = HashAlgorithm.Create("MD5");
            HashAlgorithm algorithm = (HashAlgorithm)CryptoConfig.CreateFromName("MD5");
            byte[] bytesResult = algorithm.ComputeHash(value);
            output = BitConverter.ToString(bytesResult).Replace("-", "").ToLower();
            return output;
        }

        #endregion
    }
}
