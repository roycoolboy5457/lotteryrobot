﻿using Model2.LotteryProjectModel.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model2.LotteryProjectModel
{
    public class RequestSiteBetAdd
    {
        public LotteryTypeEnum LotteryTypeId { get; set; }
        public bool StopBetIfWinning { get; set; } = false;
        public int AppendCount { get; set; } = 0;
        public List<RequestSiteBetList> Bets { get; set; }
    }
}
