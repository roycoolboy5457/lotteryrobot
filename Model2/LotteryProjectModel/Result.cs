﻿using Model2.LotteryProjectModel.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model2.LotteryProjectModel
{
    /// <summary>
    /// 返回結果。
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Result<T>
    {
        /// <summary>
        /// 建構式。
        /// </summary>
        public Result()
        {
            Guid = Guid.NewGuid();
        }

        /// <summary>
        /// 流水編號。
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// 成功狀態。
        /// </summary>
        public bool Success { get; set; } = true;

        /// <summary>
        /// 代碼。
        /// </summary>
        public string Code { get; set; } = Convert.ToInt32(ResultCode.Success).ToString();

        /// <summary>
        /// 訊息
        /// </summary>

        /// <summary>
        /// 資料。
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// 擴充資料。
        /// </summary>
        public object ExtensionData { get; set; }

        /// <summary>
        /// 分頁。
        /// </summary>
        public Pagination Pagination { get; set; }

        /// <summary>
        /// 例外。
        /// </summary>
        public Exception Exception { get; set; }
    }
}
