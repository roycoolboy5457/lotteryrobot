﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Model2.LotteryProjectModel
{
    /// <summary>
    /// 請求工具。
    /// </summary>
    public class RequestHelperStatic
    {
        static HttpClient client { get; set; }
        static HttpClientHandler httpClientHandler { get; set; }
        int timeout = 30;
        public RequestHelperStatic()
        {
            if (client == null)
            {
                httpClientHandler = new HttpClientHandler();
                client = new HttpClient(httpClientHandler);
                client.Timeout = TimeSpan.FromSeconds(timeout);
            }
        }

        /// <summary>
        /// 取得內容。
        /// </summary>
        /// <param name="url">網址。</param>
        /// <param name="content">內容。</param>
        /// <param name="header">標頭。</param>
        /// <param name="mediaType">媒體類型。</param>
        /// <returns></returns>
       	public async Task<HttpResponseMessage> GetContentAsync(HttpMethod httpMethod, string url, string content, Dictionary<string, string> header = null, string mediaType = "application/x-www-form-urlencoded")
        {
            if (httpMethod.Method != HttpMethod.Get.Method && header != null)
            {
                foreach (var item in header)
                {
                    if (item.Key == "user-agent")
                    {
                        client.DefaultRequestHeaders.UserAgent.Add(new System.Net.Http.Headers.ProductInfoHeaderValue(item.Value));
                    }
                    else if (!client.DefaultRequestHeaders.Contains(item.Key))
                    {
                        client.DefaultRequestHeaders.Add(item.Key, item.Value);
                    }
                    else if (client.DefaultRequestHeaders.Contains(item.Key))
                    {
                        client.DefaultRequestHeaders.Remove(item.Key);
                        client.DefaultRequestHeaders.Add(item.Key, item.Value);
                    }
                }
            }

            HttpResponseMessage httpResponseMessage = null;
            if (httpMethod.Method == HttpMethod.Post.Method)
            {
                httpResponseMessage = await client.PostAsync(url, new StringContent(content, Encoding.UTF8, mediaType));
            }
            else
            {
                if (!string.IsNullOrEmpty(content))
                {
                    content = content.StartsWith("?") ? content : ("?" + content);
                }

                httpResponseMessage = await client.GetAsync(url + content);
            }

            return httpResponseMessage;
        }

    }
}
