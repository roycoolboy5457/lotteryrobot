﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model2.LotteryProjectModel
{
    public class RequestSiteBetList
    {
        //玩法組合
        public int LotteryGameGroupId { get; set; }
        //玩法
        public int LotteryGameModeId { get; set; }
        //投注號碼
        public string BetNumber { get; set; }
        //注數
        public int BetCount { get; set; }
        //倍數
        public Int16 BetMultiply { get; set; }
        //金額
        //[Required]
        //[Range(0.0000, 999999999.9999)]
        public decimal BetAmount { get; set; }
        //模式 投注單位(dollar=元;dime=角;cent=分;li=厘)。
        public string BetUnitStateKey { get; set; }
        //(dollar=1;dime=0.1;cent=0.01;li=0.001)。
        public decimal BetUnit { get; set; }
        //期數
        public string RoundCode { get; set; }
    }
}
