﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model2.LotteryProjectModel.Enum
{
    /// <summary>
    /// 結果代碼。
    /// </summary>
    public enum ResultCode
    {
        /// <summary>
        /// 成功。
        /// </summary>
        [Description("Success")]
        Success = 0,

        /// <summary>
        /// 失敗。
        /// </summary>
        [Description("Fail")]
        Fail = 101,
        /// <summary>
        /// 參數錯誤。
        /// </summary>
        [Description("Parameter Error")]
        ParameterError,
        /// <summary>
        /// 資料庫錯誤。
        /// </summary>
        [Description("Database Error")]
        DatabaseError,
        /// <summary>
        /// 簽章錯誤。
        /// </summary>
        [Description("Signature Error")]
        SignatureError,
        /// <summary>
        /// 加解密錯誤。
        /// </summary>
        [Description("Encrypt Or Decrypt Error")]
        EncryptDecryptError,
        /// <summary>
        /// HttpMethod 錯誤。
        /// </summary>
        [Description("HttpMethod Error")]
        HttpMethodError,
        /// <summary>
        /// Request 錯誤。
        /// </summary>
        [Description("Request Error")]
        RequestError,
        /// <summary>
        /// 資料庫參考衝突
        /// </summary>
        [Description("Database Reference Conflict")]
        DatabaseReferenceConflict,
        /// <summary>
        /// 服務項目不存在。
        /// </summary>
        [Description("Service Item Not Exist")]
        ServiceItemNotExist = 201,
        /// <summary>
        /// 服務項目停用。
        /// </summary>
        [Description("Service Item Disable")]
        ServiceItemDisable,
        /// <summary>
        /// 服務參數不存在。
        /// </summary>
        [Description("Service Parameter Not Exist")]
        ServiceParameterNotExist,
        /// <summary>
        /// 代理不存在。
        /// </summary>
        [Description("Agent Not Exist")]
        AgentNotExist,
        /// <summary>
        /// 代理停用。
        /// </summary>
        [Description("Agent Disable")]
        AgentDisable,
        /// <summary>
        /// 商戶不存在。
        /// </summary>
        [Description("Merchant Not Exist")]
        MerchantNotExist,
        /// <summary>
        /// 商戶停用。
        /// </summary>
        [Description("Merchant Disable")]
        MerchantDisable,
        /// <summary>
        /// 服務不存在。
        /// </summary>
        [Description("Service Not Exist")]
        ServiceNotExist,
        /// <summary>
        /// 服務停用。
        /// </summary>
        [Description("Service Disable")]
        ServiceDisable,
        /// <summary>
        /// Hash Key 不存在。
        /// </summary>
        [Description("Hash Key Not Exist")]
        HashKeyNotExist,
        /// <summary>
        /// Hash 演算法不支援。
        /// </summary>
        [Description("Hash Algorithm Not Support")]
        HashAlgorithmNotSupport,
        /// <summary>
        /// 加解密 Key 不存在。
        /// </summary>
        [Description("Secret Key Not Exist")]
        SecretKeyNotExist,
        /// <summary>
        /// 加解密 IV 不存在。
        /// </summary>
        [Description("Secret IV Not Exist")]
        SecretIVNotExist,
        /// <summary>
        /// 加解密演算法不支援。
        /// </summary>
        [Description("Algorithm Not Support")]
        AlgorithmNotSupport,
        /// <summary>
        /// 服務請求頻繁
        /// </summary>
        [Description("Frequent Service Requests")]
        ServiceRequests,

        /// <summary>
        /// 帳號不存在。
        /// </summary>
        [Description("Account Not Exist")]
        AccountNotExist = 301,
        /// <summary>
        /// 帳號已存在。
        /// </summary>
        [Description("Account Exist")]
        AccountExist,
        /// <summary>
        /// 帳號錯誤。
        /// </summary>
        [Description("Account Error")]
        AccountError,
        /// <summary>
        /// 密碼錯誤。
        /// </summary>
        [Description("Password Error")]
        PasswordError,
        /// <summary>
        /// 帳號或密碼錯誤。
        /// </summary>
        [Description("Account Or Password Error")]
        AccountOrPasswordError,
        /// <summary>
        /// 帳號停用。
        /// </summary>
        [Description("Account Disable")]
        AccountDisable,
        /// <summary>
        /// 帳號凍結。
        /// </summary>
        [Description("Account Fronzen")]
        AccountFronzen,
        /// <summary>
        /// Token 錯誤。
        /// </summary>
        [Description("Token Error")]
        TokenError,
        /// <summary>
        /// 帳號未認證。
        /// </summary>
        [Description("Account Not Approve")]
        AccountNotApprove,
        /// <summary>
        /// 密碼錯誤太多次。
        /// </summary>
        [Description("Password Error Many Times")]
        PasswordErrorManyTimes,


        /// <summary>
        /// 交易編號不存在。
        /// </summary>
        [Description("Transaction SN Not Exist")]
        TransactionSNNotExist = 401,
        /// <summary>
        /// 交易編號已存在。
        /// </summary>
        [Description("Transaction SN Exist")]
        TransactionSNExist,
        /// <summary>
        /// 交易編號錯誤。
        /// </summary>
        [Description("Transaction SN Error")]
        TransactionSNError,
        /// <summary>
        /// 交易已處理。
        /// </summary>
        [Description("Transaction Processed")]
        TransactionProcessed,
        /// <summary>
        /// 交易已取消。
        /// </summary>
        [Description("Transaction Cancelled")]
        TransactionCancelled,
        /// <summary>
        /// 檢查交易狀態。
        /// </summary>
        [Description("Check Transaction Status")]
        CheckTransactionStatus,
        /// <summary>
        /// 交易失敗。
        /// </summary>
        [Description("Transaction Fail")]
        TransactionSNFail,

        /// <summary>
        /// 會員額度不存在。
        /// </summary>
        [Description("Member Credits Not Exist")]
        MemberCreditsNotExist = 501,
        /// <summary>
        /// 代理額度不存在。
        /// </summary>
        [Description("Agent Credits Not Exist")]
        AgentCreditsNotExist,
        /// <summary>
        /// 商戶額度不存在。
        /// </summary>
        [Description("Merchant Credits Not Exist")]
        MerchantCreditsNotExist,
        /// <summary>
        /// 會員額度不足。
        /// </summary>
        [Description("Member Credits Not Enough")]
        MemberCreditsNotEnough,
        /// <summary>
        /// 代理額度不足。
        /// </summary>
        [Description("Agent Credits Not Enough")]
        AgentCreditsNotEnough,
        /// <summary>
        /// 商戶額度不足。
        /// </summary>
        [Description("Merchant Credits Not Enough")]
        MerchantCreditsNotEnough,
        /// <summary>
        /// 服務額度不足。
        /// </summary>
        [Description("Service Credits Not Enough")]
        ServiceCreditsNotEnough,
        /// <summary>
        /// 平台額度不足。
        /// </summary>
        [Description("Platform Credits Not Enough")]
        PlatformCreditsNotEnough,
        /// <summary>
        /// 會員轉帳超過限制。
        /// </summary>
        [Description("Member Transfer Exceeds Limit")]
        MemberTransferExceedsLimit,
        /// <summary>
        /// 代理轉帳超過限制。
        /// </summary>
        [Description("Agent Transfer Exceeds Limit")]
        AgentTransferExceedsLimit,
        /// <summary>
        /// 商戶轉帳超過限制。
        /// </summary>
        [Description("Merchant Transfer Exceeds Limit")]
        MerchantTransferExceedsLimit,
        /// <summary>
        /// 服務轉帳超過限制。
        /// </summary>
        [Description("Service Transfer Exceeds Limit")]
        ServiceTransferExceedsLimit,
        /// <summary>
        /// 平台轉帳超過限制。
        /// </summary>
        [Description("Platform Transfer Exceeds Limit")]
        PlatformTransferExceedsLimit,


        /// <summary>
        /// 語系代碼錯誤。
        /// </summary>
        [Description("Language Code Error")]
        LanguageCodeError = 601,
        /// <summary>
        /// 幣別代碼錯誤。
        /// </summary>
        [Description("Currency Code Error")]
        CurrencyCodeError,
        /// <summary>
        /// 遊戲代碼錯誤。
        /// </summary>
        [Description("Game Code Error")]
        GameCodeError,
        /// <summary>
        /// 銀行代碼錯誤。
        /// </summary>
        [Description("Bank Code Error")]
        BankCodeError,


        /// <summary>
        /// 請再試一次。
        /// </summary>
        [Description("Try Again")]
        TryAgain = 701,
        /// <summary>
        /// 處理中。
        /// </summary>
        [Description("Processing")]
        Processing,
        /// <summary>
        /// 忙碌中。
        /// </summary>
        [Description("Busy")]
        Busy,
        /// <summary>
        /// 遊戲中。
        /// </summary>
        [Description("Playing")]
        Playing,
        /// <summary>
        /// 呼叫太多次。
        /// </summary>
        [Description("Request Too Many Times")]
        RequestTooManyTimes,
        /// <summary>
        /// 過期。
        /// </summary>
        [Description("Expired")]
        Expired,


        /// <summary>
        /// 連線異常。
        /// </summary>
        [Description("Connection Error")]
        ConnectionError = 801,
        /// <summary>
        /// 網路異常。
        /// </summary>
        [Description("Network Error")]
        NetworkError,
        /// <summary>
        /// 逾時。
        /// </summary>
        [Description("Timeout")]
        Timeout,


        /// <summary>
        /// 子項目存在。
        /// </summary>
        [Description("Sub Item Exist")]
        SubItemExist = 5001,
        /// <summary>
        /// 關鍵字已存在
        /// </summary>
        [Description("Keyword  Exist")]
        KeywordExist = 5002,


        /// <summary>
        /// 沒有權限。
        /// </summary>
        [Description("Forbidden")]
        Forbidden = 8001,


        /// <summary>
        /// 設定維護。
        /// </summary>
        [Description("Maintenance")]
        Maintenance = 9990,
        /// <summary>
        /// 網站維護。
        /// </summary>
        [Description("Website Maintenance")]
        WebsiteMaintenance = 9991,
        /// <summary>
        /// 線路維護。
        /// </summary>
        [Description("Line Maintenance")]
        LineMaintenance = 9992,
        /// <summary>
        /// 平台維護。
        /// </summary>
        [Description("Platform Maintenance")]
        PlatformMaintenance = 9993,
        /// <summary>
        /// 商戶服務維護。
        /// </summary>
        [Description("MerchantService Maintenance")]
        MerchantServiceMaintenance = 9994,
        /// <summary>
        /// 服務維護。
        /// </summary>
        [Description("Service Maintenance")]
        ServiceMaintenance = 9995,
        /// <summary>
        /// 方案維護。
        /// </summary>
        [Description("Project Maintenance")]
        ProjectMaintenance = 9996,
        /// <summary>
        /// IP 不允許。
        /// </summary>
        [Description("IP Not Allow")]
        IPNotAllow = 9997,
        /// <summary>
        /// 預期外的錯誤。
        /// </summary>
        [Description("Unknown")]
        Unknown = 9998,
        /// <summary>
        /// 內部錯誤。
        /// </summary>
        [Description("Internal Error")]
        InternalError = 9999,
        /// <summary>
        /// 未準備。
        /// </summary>
        [Description("Not Ready")]
        NotReady = 10000,


        /// <summary>
        /// 銷售區間超過限制。
        /// </summary>
        [Description("Sale Periods Exceeds Limit")]
        SalePeriodsExceedsLimit,

        /// <summary>
        /// 時間區間有誤。
        /// </summary>
        [Description("Time Range Wrong")]
        TimeRangeWrong,

        /// <summary>
        /// 資料已存在。
        /// </summary>
        [Description("Data Exist")]
        DataExist,

        /// <summary>
        /// 異常資料不存在。
        /// </summary>
        [Description("Exception Item Not Exist")]
        ExceptionItemNotExist,

        /// <summary>
        /// 操作步驟錯誤。
        /// </summary>
        [Description("Operate Flow Wrong")]
        OperateFlowWrong,

        /// <summary>
        /// 開獎號碼格式錯誤。
        /// </summary>
        [Description("DrawNumberFormatWrong")]
        DrawNumberFormatWrong,


        /// <summary>
        /// 彩種不存在。
        /// </summary>
        [Description("Lottery Type Not Exist")]
        LotteryTypeNotExist,


        /// <summary>
        /// 該獎期存在未審核異常。
        /// </summary>
        [Description("Lottery Round Exist Unreview")]
        LotteryRoudExistUnreview,

        /// <summary>
        /// 該獎期資料不存在。
        /// </summary>
        [Description("Lottery Round Not Exist")]
        LotteryRoudNotExist,

        /// <summary>
        /// 異常原因錯誤。
        /// </summary>
        [Description("ExceptionStatusWrong")]
        ExceptionStatusWrong,

        /// <summary>
        /// 該獎期派獎中。
        /// </summary>
        [Description("InTheArard")]
        InTheAward,

        /// <summary>
        /// 撤單資料不存在。
        /// </summary>
        [Description("BetBackout Not Exist")]
        BetBackoutNotExist,

        /// <summary>
        /// 該獎期存在未審核撤單。
        /// </summary>
        [Description("BetBackout Exist Unreview")]
        BetBackoutExistUnreview,

        /// <summary>
        /// 獎期區間有誤。
        /// </summary>
        [Description("Round Code Range Wrong")]
        RoundCodeRangeWrong,

        /// <summary>
        /// 該獎期存在未審核手動開獎單。
        /// </summary>
        [Description("Reopening Exist Unreview")]
        ReopeningExistUnreview,

        /// <summary>
        /// 手動開獎單資料不存在。
        /// </summary>
        [Description("Reopening Not Exist")]
        ReopeningNotExist,

        /// <summary>
        /// 撤單範圍格式錯誤。
        /// </summary>
        [Description("BetBackout Target Format Wrong")]
        BetBackoutTargetFormatWrong,

        /// <summary>
        /// 撤單失敗。
        /// </summary>
        [Description("BetBackout Fail")]
        BetBackoutFail,

        /// <summary>
        /// 重試間隔秒數錯誤。
        /// </summary>
        [Description("Retry Interval Seconds Wrong")]
        RetryIntervalSecondsWrong,

        /// <summary>
        /// 排程重啟失敗。
        /// </summary>
        [Description("Crawler Restart Fail")]
        CrawlerRestartFail,

        /// <summary>
        /// 上傳格式錯誤。
        /// </summary>
        [Description("Upload Format Wrong")]
        UploadFormatWrong,

        /// <summary>
        /// 資料不存在。
        /// </summary>
        [Description("Data Not Exist")]
        DataNotExist,

        /// <summary>
        /// 該角色未啟用。
        /// </summary>
        [Description("Role Is Disabled")]
        RoleIsDisabled,

        /// <summary>
        /// 資料未變更。
        /// </summary>
        [Description("Data None Changing")]
        DataNoneChanging,

        /// <summary>
        /// 重開獎失敗。
        /// </summary>
        [Description("Apply Reopening Error")]
        ApplyReopeningError,

        /// <summary>
        /// 設定為刪除必須先設定為未啟用。
        /// </summary>
        [Description("Enable False Is Necessity")]
        EnableFalseIsNecessity,

        /// <summary>
        /// 下注格式錯誤
        /// </summary>
        [Description("Bet Format Error")]
        BetFormatError,

        /// <summary>
        /// 下注注式錯誤
        /// </summary>
        [Description("Bet Count Error")]
        BetCountError,

        /// <summary>
        /// 下注金額錯誤
        /// </summary>
        [Description("Bet Cost Error")]
        BetCostError,

        /// <summary>
        /// 商家已刪除
        /// </summary>
        [Description("Dealer Is Deleted")]
        DealerIsDeleted,

        /// <summary>
        /// 商家不存在
        /// </summary>
        [Description("Dealer Not Exist")]
        DealerNotExist,

        /// <summary>
        /// 商家停用
        /// </summary>
        [Description("Dealer Disable")]
        DealerDisable,

        /// <summary>
        /// 獎期已停售
        /// </summary>
        [Description("Lottery Round Stop Selling")]
        LotteryRoundStopSelling,
        /// <summary>
        /// 玩法單注最高的限制額度
        /// </summary>
        [Description("Game Mode Bet Limit")]
        GameModeBetlimit,
        /// <summary>
        /// 倍投限额
        /// </summary>
        [Description("Bet Multiply Upper Limit")]
        BetMultiplyUpperlimit,
        /// <summary>
        /// 倍數最大限制
        /// </summary>
        [Description("Bet Multiply Limit")]
        BetMultiplylimit,
        /// <summary>
        /// 系統金額最大限制
        /// </summary>
        [Description("System Bet Limit")]
        SystemBetlimit,

        /// <summary>
        /// 管理者已刪除
        /// </summary>
        [Description("Manager Is Deleted")]
        ManagerIsDeleted,

        /// <summary>
        /// 管理者不存在
        /// </summary>
        [Description("Manager Not Exist")]
        ManagerNotExist,

        /// <summary>
        /// 管理者停用
        /// </summary>
        [Description("Manager Disable")]
        ManagerDisable,

        /// <summary>
        /// 角色名稱重複
        /// </summary>
        [Description("RoleDescriptionIsExist")]
        RoleDescriptionIsExist,

        /// <summary>
        /// 管理者角色設定錯誤
        /// </summary>
        [Description("ManagerRoleInfoSettingError")]
        ManagerRoleInfoSettingError,
        /// <summary>
        /// 玩法維護中
        /// </summary>
        [Description("GameModeMaintenance")]
        GameModeMaintenance
    }
}
