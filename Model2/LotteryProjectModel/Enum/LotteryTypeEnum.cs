﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model2.LotteryProjectModel.Enum
{
    public enum LotteryTypeEnum
    {
        [Description("無")]
        None = -1,
        [Description("重慶時時彩")]
        ChongqingSSC = 1,
        [Description("新疆時時彩")]
        XinjiangSSC = 15,
        [Description("天津時時彩")]
        TianjinSSC = 14,
        [Description("北京PK拾")]
        BeijingPKTen = 6,
        [Description("北京快樂八")]
        BeijingHappyEight = 10,
        [Description("加拿大 3.5分")]
        CanadaThreeFive = 13
    }
}
