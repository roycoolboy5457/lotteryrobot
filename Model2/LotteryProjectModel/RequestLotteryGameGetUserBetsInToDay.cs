﻿using Model2.LotteryProjectModel.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model2.LotteryProjectModel
{
    public class RequestLotteryGameGetUserBetsInToDay
    {
        public LotteryTypeEnum LotteryTypeId { get; set; }
    }
}
