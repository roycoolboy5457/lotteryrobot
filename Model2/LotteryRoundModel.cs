﻿using Model2.LotteryProjectModel.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model2
{
    public class LotteryRoundModel
    {
        public LotteryTypeEnum LotteryTypeId { get; set; }
        public string RoundCode { get; set; }
        public DateTime IssueDateTime { get; set; }
        public DateTime SaleDeadline { get; set; }
    }
}
