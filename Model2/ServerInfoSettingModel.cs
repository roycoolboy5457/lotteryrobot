﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model2
{
    public class ServerInfoSettingModel
    {
        public static string Ip { get; set; } //= "https://www-api.rgq58.com/API";
        public static string FilePath { get; set; } = System.Environment.CurrentDirectory + "\\Setting_ServerInfo.txt";

        public static string MD5Hash { get; set; } = "hSf~kd@dj(sM<dsf/Xd@djPZ[fkj*zqW";
    }
}
