﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model2
{
    public class AddManyBetsAsyncCallApiModel
    {
        public static string Url { get; set; } = $"{ServerInfoSettingModel.Ip}/LotteryGame/AddManyBetsAsync";
        public static long SucessCount { get; set; } = 0;
        public static long ErrorCount { get; set; } = 0;
        public static long WarningCount { get; set; } = 0;
        public static long AvgSucessSpanTime { get; set; } = 0;
    }
}
