﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model2
{
    public class MessageModel
    {
        public DateTime dateTime { get; set; }
        public string message { get; set; }
        public MessageEnum messageEnum { get; set; }
        public string dateTimeToString
        {
            get { return dateTime.ToString("MM/dd HH:mm:ss"); ; }
        }
    }
}
