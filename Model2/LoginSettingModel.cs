﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model2
{
    public class LoginSettingModel
    {
        public static string loginUrl
        {
            get { return $"{ServerInfoSettingModel.Ip}/Authenticate/ValidateAsync"; }
        } 
        public static string userFilePath { get; set; } = System.Environment.CurrentDirectory + "\\Setting_User.txt";
    }
}
