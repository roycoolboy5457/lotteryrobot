﻿using Service2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace LotteryRobot.ControllerService
{
    public class MainWindowControllerFacade 
    {
        ListBox messageController;
        ListBox errorMessageController;
        protected Window window;
        protected int second = 5;

        public MainWindowControllerFacade(Window window, ListBox MessageCntroller)
        {
            this.window = window;
            this.messageController = MessageCntroller;
        }

        public void polling()
        {
            Task task = new Task(() =>
            {
                while (true)
                {
                    try
                    {
                        long AddCountManyBetsAsyncCallApi = (AddManyBetsAsyncCallApi.logic.SucessCount - AddManyBetsAsyncCallApi.logic.LastSucessCount) / second;
                        long AddCountGetUserBetsRecentlyCallApi = (GetUserBetsRecentlyCallApi.logic.SucessCount - GetUserBetsRecentlyCallApi.logic.LastSucessCount) / second;
                        long AddCountGetUserBetsInTodayCallApi = (GetUserBetsInTodayCallApi.logic.SucessCount - GetUserBetsInTodayCallApi.logic.LastSucessCount) / second;
                        long AddCountHomeGetCallApi = (HomeGetCallApi.logic.SucessCount - HomeGetCallApi.logic.LastSucessCount) / second;

                        AddManyBetsAsyncCallApi.logic.LastSucessCount = AddManyBetsAsyncCallApi.logic.SucessCount;
                        GetUserBetsRecentlyCallApi.logic.LastSucessCount = GetUserBetsRecentlyCallApi.logic.SucessCount;
                        GetUserBetsInTodayCallApi.logic.LastSucessCount = GetUserBetsInTodayCallApi.logic.SucessCount;
                        HomeGetCallApi.logic.LastSucessCount = HomeGetCallApi.logic.SucessCount;

                        List<string> MessageList = new List<string>()
                        {
                            $"AddManyBetsAsync___________成功:{AddManyBetsAsyncCallApi.logic.SucessCount} 失敗:{AddManyBetsAsyncCallApi.logic.ErrorCount} 警告:{AddManyBetsAsyncCallApi.logic.WarningCount} 增加次數(秒):{AddCountManyBetsAsyncCallApi} 平均反應時間:{AddManyBetsAsyncCallApi.logic.AvgSucessSpanTime}",
                            $"GetUserBetsRecentlyCallApi_成功:{GetUserBetsRecentlyCallApi.logic.SucessCount} 失敗:{GetUserBetsRecentlyCallApi.logic.ErrorCount} 警告:{GetUserBetsRecentlyCallApi.logic.WarningCount} 增加次數(秒):{AddCountGetUserBetsRecentlyCallApi} 平均反應時間:{GetUserBetsRecentlyCallApi.logic.AvgSucessSpanTime}",
                            $"GetUserBetsInTodayCallApi__成功:{GetUserBetsInTodayCallApi.logic.SucessCount} 失敗:{GetUserBetsInTodayCallApi.logic.ErrorCount} 警告:{GetUserBetsInTodayCallApi.logic.WarningCount} 增加次數(秒):{AddCountGetUserBetsInTodayCallApi} 平均反應時間:{GetUserBetsInTodayCallApi.logic.AvgSucessSpanTime}",
                            $"HomeGetCallApi_____________成功:{HomeGetCallApi.logic.SucessCount} 失敗:{HomeGetCallApi.logic.ErrorCount} 警告:{HomeGetCallApi.logic.WarningCount} 增加次數(秒):{AddCountHomeGetCallApi} 平均反應時間:{HomeGetCallApi.logic.AvgSucessSpanTime}",
                        };
                        this.window.Dispatcher.BeginInvoke(new Action(() =>
                        {
                            this.messageController.Items.Clear();
                            foreach (var item in MessageList)
                            {

                                this.messageController.Items.Add(new ListBoxItem { Content = item, Foreground = Brushes.Black });

                            }
                        }));
                        Thread.Sleep(second * 1000);
                    }
                    catch (Exception ex)
                    {
                        
                    }

                }
            });
            task.Start();
        }
    }
}
