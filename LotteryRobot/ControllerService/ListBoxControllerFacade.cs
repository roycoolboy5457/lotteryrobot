﻿using Model2;
using Service2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace LotteryRobot.ControllerService
{
    public class ListBoxControllerFacade
    {
        ListBox controller;
        Window window;
        int second = 3;

        public ListBoxControllerFacade(Window window, ListBox aimCntroller)
        {
            this.window = window;
            this.controller = aimCntroller;
        }

        public void refleshView(List<string> data)
        {
            this.window.Dispatcher.BeginInvoke(new Action(() =>
            {
                this.controller.Items.Clear();
                foreach (var item in data)
                {
                    this.controller.Items.Add(new ListBoxItem { Content = item, Foreground = Brushes.Black });
                }
            }));
        }

    }

}
