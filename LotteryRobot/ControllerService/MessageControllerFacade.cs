﻿using Model2;
using Service2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace LotteryRobot.ControllerService
{
    public class MessageControllerFacade
    {
        ListBox messageController;
        ListBox errorMessageController;
        protected Window window;
        protected List<MessageService> messageServiceList = new List<MessageService>();
        protected int second = 3;

        public MessageControllerFacade(Window window, ListBox MessageCntroller, ListBox ErrorMessageCntroller)
        {
            this.window = window;
            this.messageController = MessageCntroller;
            this.errorMessageController = ErrorMessageCntroller;
        }

        public void addSource(MessageService messageService)
        {
            messageServiceList.Add(messageService);
        }

        public void polling()
        {
            Task task = new Task(() =>
            {
                long messageIndex = 1;
                long errorMessageIndex = 1;
                while (true)
                {
                    List<MessageModel> tempMessageInfoList = new List<MessageModel>();

                    foreach (var item in messageServiceList)
                    {
                        var data = item.getMessageDataThenClearData();
                        tempMessageInfoList.AddRange(data);
                    }

                    if (tempMessageInfoList.Count == 0)
                        continue;

                    tempMessageInfoList = tempMessageInfoList.OrderBy(r => r.dateTime).ToList();

                    foreach (var item in tempMessageInfoList)
                    {
                        SolidColorBrush brushes = Brushes.Black;
                        switch (item.messageEnum)
                        {
                            case MessageEnum.Info:
                                brushes = Brushes.Black;
                                break;
                            case MessageEnum.Warning:
                                brushes = Brushes.Orange;
                                break;
                            case MessageEnum.Error:
                                brushes = Brushes.Red;
                                break;

                        }

                        this.window.Dispatcher.BeginInvoke(new Action(() =>
                        {
                            if (this.messageController.Items.Count > 100)
                            {
                                this.messageController.Items.Clear();
                            }

                            if (this.errorMessageController.Items.Count > 100)
                            {
                                this.errorMessageController.Items.Clear();
                            }

                            this.messageController.Items.Add(new ListBoxItem { Content = $"{messageIndex++}.{item.dateTimeToString} {item.message}", Foreground = brushes });
                            if (item.messageEnum == MessageEnum.Warning || item.messageEnum == MessageEnum.Error)
                            {
                                this.errorMessageController.Items.Add(new ListBoxItem { Content = $"{errorMessageIndex++}.{item.dateTimeToString} {item.message}", Foreground = brushes });
                            }
                        }));

                    }

                    Thread.Sleep(second * 1000);
                }
            });
            task.Start();
        }
    }
}
