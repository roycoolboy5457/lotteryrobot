﻿using LotteryRobot.ControllerService;
using Service2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LotteryRobot
{
    /// <summary>
    /// LoginWindow.xaml 的互動邏輯
    /// </summary>
    public partial class LoginWindow : Window
    {
        MainWindow parentWindow;
        Button button;
        ListBoxControllerFacade listBoxControllerFacade; 
        public LoginWindow(MainWindow parrentWindow, Button parrentButton)
        {
            InitializeComponent();

            listBoxControllerFacade = new ListBoxControllerFacade(this,this.ListBoxMessage);
            //關閉按鈕
            this.parentWindow = parrentWindow;
            this.button = parrentButton;
            this.button.IsEnabled = false;
            this.BtnLogin.IsEnabled = !LoginService.Instance.isStart;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            this.parentWindow.callBackCloseWindow(this.button);
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            this.BtnLogin.IsEnabled = false;
            Task task1 = new Task(async () => LoginService.Instance.loginAsync());
            task1.Start();
        }

        private void BetShowUsers_Click(object sender, RoutedEventArgs e)
        {
            var index = 1;
            var messageList = LoginService.Instance.Users.Select(r => $"{index++}玩家帳號:{r.account.ToString()}").ToList();
            listBoxControllerFacade.refleshView(messageList);
        }
    }
}
