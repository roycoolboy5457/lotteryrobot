﻿using LotteryRobot.ControllerService;
using Model2.LotteryProjectModel.Enum;
using Service2;
using Service2.Extentstion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LotteryRobot
{
    /// <summary>
    /// BetWindow.xaml 的互動邏輯
    /// </summary>
    public partial class BetWindow : Window
    {
        LotteryTypeEnum currentLotteryTypeEnum;
        MessageControllerFacade messageFacade;
        BetService betService = new BetService();
        public BetWindow()
        {
            InitializeComponent();
            initComboBoxLotteryType();

            messageFacade = new MessageControllerFacade(this, this.ListBoxMessage, this.ListBoxErrorMessage);
            messageFacade.addSource(betService.MessageService);
            messageFacade.polling();
        }

        private void initComboBoxLotteryType()
        {
            foreach (LotteryTypeEnum item in Enum.GetValues(typeof(LotteryTypeEnum)))
            {
                this.comboBoxLotteryType.Items.Add(new ComboBoxItem()
                {
                    Content = item.Description(),
                    Tag = item
                });
            }
        }

        private void BtnBet_Click(object sender, RoutedEventArgs e)
        {
            BtnBet.IsEnabled = false;
            var betPollingSecond = int.Parse(txBetTimeSpan.Text);
            var skipCount = int.Parse(txSkipUserCount.Text.ToString());
            var takeCOunt = int.Parse(txTakeUserCount.Text.ToString());

            Task task2 = new Task(async () => await betService.pool(currentLotteryTypeEnum, betPollingSecond, skipCount, takeCOunt));
            task2.Start();
        }
 
        private void ComboBoxLotteryType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.currentLotteryTypeEnum = (LotteryTypeEnum)(((ComboBox)sender).SelectedItem as ComboBoxItem).Tag;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            betService.stopPolling();
        }
    }
}
