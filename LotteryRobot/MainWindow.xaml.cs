﻿using LotteryRobot.ControllerService;
using Model2;
using Service2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LotteryRobot
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        MessageControllerFacade messageFacade;
        MainWindowControllerFacade mainWindowControllerFacade;
        public MainWindow()
        {
            InitializeComponent();

            ServerService.Instance.load();

            messageFacade = new MessageControllerFacade(this, this.ListBoxMessage,this.ListBoxErrorMessage);
            messageFacade.addSource(LoginService.Instance.MessageService);
            messageFacade.addSource(LotteryRoundService.Instance.MessageService);
            messageFacade.addSource(ServerService.Instance.MessageService);
            messageFacade.polling();

            mainWindowControllerFacade = new MainWindowControllerFacade(this, this.ListBoxApiState);
            mainWindowControllerFacade.polling();

            LotteryRoundService.Instance.polling();
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
           new LoginWindow(this, (Button)sender).Show();
        }

        public void callBackCloseWindow(Button button)
        {
            button.IsEnabled = true;
        }

        private void BtnBet_Click(object sender, RoutedEventArgs e)
        {
            new BetWindow().Show();
        }
    }
}
