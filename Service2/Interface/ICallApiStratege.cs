﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service2.Interface
{
    public interface ICallApiStratege
    {
        //Task<string> CallApi<T>(T input);
        Task CallApiAsync();
        string getUrl();
    }
}
