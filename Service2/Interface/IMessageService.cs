﻿using Model2;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service2.Interface
{
    public interface IMessageService
    {
        bool addMessage(MessageEnum messageEnum ,string message);
        List<MessageModel> getMessageDataThenClearData();
    }
}
