﻿using Model2;
using Model2.LotteryProjectModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Service2
{
    public class ServerService
    {
        private static volatile ServerService instance;
        private LoginSettingModel loginSettingModel = new LoginSettingModel();
        private static object initLocker = new Object();
        public MessageService MessageService = new MessageService();

        public static ServerService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (initLocker)
                    {
                        if (instance == null)
                        {
                            instance = new ServerService();
                        }
                    }
                }

                return instance;
            }
        }

        private ServerService() { }

        public void load()
        {
            try
            {
                var result = new CsvService().ReadCSV(ServerInfoSettingModel.FilePath);
                var Ip = result.Rows[0]["IP"].ToString();
                ServerInfoSettingModel.Ip = Ip;
                MessageService.addMessage(MessageEnum.Info, $"ServerIP:{Ip}");
            }
            catch (Exception ex)
            {
                MessageService.addMessage(MessageEnum.Error,$"ServerIP 讀取檔案失敗:{ex.ToString()}");
            }
        }

    }
}
