﻿using Model2;
using Service2.Interface;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service2
{
    public class MessageService : IMessageService
    {
        public object locker = new object();

        public BlockingCollection<MessageModel> data = new BlockingCollection<MessageModel>();

        public bool addMessage(MessageEnum messageEnum, string message)
        {
            //lock (locker)
            //{
                data.Add(new MessageModel()
                {
                    dateTime = DateTime.Now,
                    message = message,
                    messageEnum = messageEnum
                });
            //}

            return true;
        }

        public List<MessageModel> getMessageDataThenClearData()
        {
            List<MessageModel> result = new List<MessageModel>();
            if (data.Count > 0)
            {
                lock (locker)
                {
                    result = data.ToList();
                    data = new BlockingCollection<MessageModel>();
                }
            }
            return result;
        }
    }
}
