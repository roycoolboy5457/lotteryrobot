﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service2.Extentstion
{
    public static class EnumExtension
    {
        /// <summary>
        /// 取得屬性中的描述。
        /// </summary>
        /// <param name="source">列舉項目。</param>
        /// <returns></returns>
        public static string Description<TSource>(this TSource source)
        {
            var descriptionAttribute = (DescriptionAttribute)source.GetType()
                    .GetField(source.ToString())
                    .GetCustomAttributes(false)
                    .Where(a => a is DescriptionAttribute)
                    .FirstOrDefault();

            return descriptionAttribute != null ? descriptionAttribute.Description : source.ToString();
        }
    }
}
