﻿using Model2;
using Model2.LotteryProjectModel;
using Service2.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service2
{
    public class PatternCallApi 
    {
        public long SucessCount { get; set; } = 0;
        public long ErrorCount { get; set; } = 0;
        public long WarningCount { get; set; } = 0;
        public long AvgSucessSpanTime { get; set; } = 0;
        public long TotalSpanElapsedTime { get; set; } = 0;
        public long LastSucessCount { get; set; } = 0;

        public string createSignature(string source)
        {
            return new MD5Helper().Encrypt(Encoding.UTF8.GetBytes(source + ServerInfoSettingModel.MD5Hash));
        }
    }
}
