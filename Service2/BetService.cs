﻿using Model2;
using Model2.LotteryProjectModel;
using Model2.LotteryProjectModel.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Service2.Extentstion;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Service2
{
    public class BetService
    {
        private static object initLocker = new Object();
        public MessageService MessageService = new MessageService();
        long total = 0;
        Stopwatch sw = new Stopwatch();
        bool isPolling = true;

        public async Task pool(LotteryTypeEnum lotteryTypeEnum,int pollingSeconds,int skipCount,int takeCount)
        {
            var users = LoginService.Instance.Users.Skip(skipCount).Take(takeCount).ToList();
            var pollingThreshold = pollingSeconds * 1;

            foreach (var item in users)
            {
                Task task2 = new Task(async () => BetFlow(item, lotteryTypeEnum, pollingThreshold));
                task2.Start();

                Thread.Sleep(100);
            }
        }

        private async Task BetFlow(UserModel item, LotteryTypeEnum lotteryTypeEnum, int pollingThreshold)
        {
            long betCount = 0;
            RequestHelper requestHelper = new RequestHelper();
            MessageService.addMessage(MessageEnum.Info, $"{item.account}開始下注");
            while (isPolling)
            {
                try
                {
                    betCount++;
                    var currentRoundModel = LotteryRoundService.Instance.getCurrentRound(lotteryTypeEnum);

                    if (currentRoundModel == null)
                        MessageService.addMessage(MessageEnum.Warning,$"{lotteryTypeEnum.Description()}取不到當前獎期");


                    await new AddManyBetsAsyncCallApi(item, requestHelper, currentRoundModel.RoundCode, lotteryTypeEnum, betCount, MessageService).CallApiAsync();
                    //await new GetUserBetsInTodayCallApi(item, requestHelper, lotteryTypeEnum, MessageService).CallApiAsync();
                    //await new GetUserBetsRecentlyCallApi(item, requestHelper, lotteryTypeEnum, MessageService).CallApiAsync();
                    //await new HomeGetCallApi(item, requestHelper, MessageService).CallApiAsync();
                }
                catch (Exception ex)
                {
                    MessageService.addMessage(MessageEnum.Warning,$"下注流程發生錯誤 {ex.ToString()}");
                }

                Thread.Sleep(pollingThreshold);
            }
        }

        private RequestSiteBetList setLotteryBetInfo(LotteryTypeEnum lotteryTypeEnum, string currentRoundCode, long betCount)
        {
            var result = new RequestSiteBetList();

            var isWin = false;
            if (betCount % 10 == 0)
                isWin = true;

            if (lotteryTypeEnum == LotteryTypeEnum.ChongqingSSC)
            {
                if (isWin)
                {
                    result.LotteryGameGroupId = 10;
                    result.LotteryGameModeId = 93;
                    result.BetNumber = "Dragon Tiger Flat";
                    result.BetMultiply = 1;
                    result.BetUnitStateKey = "dollar";
                    result.BetCount = 3;
                    result.BetAmount = 6;
                    result.BetUnit = 1;
                }
                else
                {
                    result.LotteryGameGroupId = 1;
                    result.LotteryGameModeId = 1;
                    result.BetNumber = "7|4|9|0|8";
                    result.BetMultiply = 1;
                    result.BetUnitStateKey = "dollar";
                    result.BetCount = 1;
                    result.BetAmount = 2;
                    result.BetUnit = 1;
                    result.RoundCode = "20200108034";
                }

            }
            else if (lotteryTypeEnum == LotteryTypeEnum.TianjinSSC)
            {
                if (isWin)
                {
                    result.LotteryGameGroupId = 43;
                    result.LotteryGameModeId = 365;
                    result.BetNumber = "Dragon Tiger Flat";
                    result.BetMultiply = 1;
                    result.BetUnitStateKey = "dollar";
                    result.BetCount = 3;
                    result.BetAmount = 6;
                    result.BetUnit = 1;
                }
                else
                {
                    result.BetAmount = 2;
                    result.BetCount = 1;
                    result.BetMultiply = 1;
                    result.BetNumber = "3|1|0|7|0";
                    result.BetUnit = 1;
                    result.BetUnitStateKey = "dollar";
                    result.LotteryGameGroupId = 34;
                    result.LotteryGameModeId = 300;
                }
            }
            else if (lotteryTypeEnum == LotteryTypeEnum.XinjiangSSC)
            {
                if (isWin)
                {
                    result.LotteryGameGroupId = 55;
                    result.LotteryGameModeId = 265;
                    result.BetNumber = "Dragon Tiger Flat";
                    result.BetMultiply = 1;
                    result.BetUnitStateKey = "dollar";
                    result.BetCount = 3;
                    result.BetAmount = 6;
                    result.BetUnit = 1;
                }
                else
                {
                    result.BetAmount = 2;
                    result.BetCount = 1;
                    result.BetMultiply = 1;
                    result.BetNumber = "2|5|4|0|5";
                    result.BetUnit = 1;
                    result.BetUnitStateKey = "dollar";
                    result.LotteryGameGroupId = 46;
                    result.LotteryGameModeId = 200;
                }
            }
            else if (lotteryTypeEnum == LotteryTypeEnum.BeijingPKTen)
            {
                if (isWin)
                {
                    result.LotteryGameGroupId = 19;
                    result.LotteryGameModeId = 81;
                    result.BetNumber = "Dragon Tiger";
                    result.BetMultiply = 1;
                    result.BetUnitStateKey = "dollar";
                    result.BetCount = 2;
                    result.BetAmount = 4;
                    result.BetUnit = 1;
                    result.RoundCode = "743254";
                }
                else
                {
                    result.BetAmount = 2;
                    result.BetCount = 1;
                    result.BetMultiply = 1;
                    result.BetNumber = "Small";
                    result.BetUnit = 1;
                    result.BetUnitStateKey = "dollar";
                    result.LotteryGameGroupId = 14;
                    result.LotteryGameModeId = 82;
                }

            }
            else if (lotteryTypeEnum == LotteryTypeEnum.BeijingHappyEight)
            {
                if (isWin)
                {
                    result.LotteryGameGroupId = 20;
                    result.LotteryGameModeId = 90;
                    result.BetNumber = "Upper Middle Lower";
                    result.BetMultiply = 1;
                    result.BetUnitStateKey = "dollar";
                    result.BetCount = 3;
                    result.BetAmount = 6;
                    result.BetUnit = 1;
                    result.RoundCode = "991766";
                }
                else
                {
                    result.BetAmount = 16;
                    result.BetCount = 8;
                    result.BetMultiply = 1;
                    result.BetNumber = "01 02 21 22|41 42 61 62";
                    result.BetUnit = 1;
                    result.BetUnitStateKey = "dollar";
                    result.LotteryGameGroupId = 21;
                    result.LotteryGameModeId = 89;
                }
            }
            result.RoundCode = currentRoundCode;
            return result;
        }

        public void stopPolling()
        {
            isPolling = false;
        }
    }
}
