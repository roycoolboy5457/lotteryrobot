﻿using Model2;
using Model2.LotteryProjectModel;
using Model2.LotteryProjectModel.Enum;
using Newtonsoft.Json;
using Service2.Interface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Service2
{
    public class AddManyBetsAsyncCallApi : ICallApiStratege
    {
        public static PatternCallApi logic = new PatternCallApi();

        Stopwatch sw = new Stopwatch();
        UserModel userInfo;
        RequestHelper requestHelper;
        string currentRoundCode;
        LotteryTypeEnum lotteryTypeEnum;
        long betCount;
        MessageService messageService;
        public AddManyBetsAsyncCallApi(UserModel userInfo, RequestHelper requestHelper, string currentRoundCode, LotteryTypeEnum lotteryTypeEnum, long betCount, MessageService messageService)
        {
            this.userInfo = userInfo;
            this.requestHelper = requestHelper;
            this.currentRoundCode = currentRoundCode;
            this.lotteryTypeEnum = lotteryTypeEnum;
            this.betCount = betCount;
            this.messageService = messageService;
        }

        public async Task CallApiAsync() 
        {
            try
            {
                var source = createSource();
                var signature = logic.createSignature(source);
                var authorization = $"bearer {userInfo.token}";

                sw.Reset();
                sw.Start();
                var response = await callApiAsync(source, authorization, signature);
                var responseBody = await response.Content.ReadAsStringAsync();
                sw.Stop();

                recodeMessage(responseBody);
            }
            catch (Exception ex)
            {
                messageService.addMessage(MessageEnum.Error, $"bet {ex.ToString()}");
                AddManyBetsAsyncCallApiModel.ErrorCount++;
            }
        }

        protected void recodeMessage(string responseBody)
        {
            //messageService.addMessage(MessageEnum.Info, responseBody);

            if ((!responseBody.Contains($"\"Code\":\"0\"") && !responseBody.Contains($"\"Code\":\"10033\"")) || (responseBody.Contains($"\"Alert\"")))
            {
                messageService.addMessage(MessageEnum.Warning, $"bet {responseBody.ToString()}");
                logic.WarningCount++;
            }
            else
            {
                logic.SucessCount++;
                logic.TotalSpanElapsedTime += sw.ElapsedMilliseconds;
                logic.AvgSucessSpanTime = logic.TotalSpanElapsedTime / logic.SucessCount;
            }
        }

        protected async Task<HttpResponseMessage> callApiAsync(string source, string authorization, string signature)
        {
            return await requestHelper.GetContentAsync(
                   HttpMethod.Post,
                   getUrl(),
                   source,
                   header: new System.Collections.Generic.Dictionary<string, string>() { { "Authorization", authorization }, { "signature", System.Net.WebUtility.UrlEncode(signature) } },
                   mediaType: "application/json"
               );
        }

        protected string createSource()
        {
            var bet = setLotteryBetInfo(lotteryTypeEnum, currentRoundCode, betCount);
            var param = new RequestSiteBetAdd()
            {
                LotteryTypeId = lotteryTypeEnum,
                AppendCount = 0,
                StopBetIfWinning = false,
                Bets = new List<RequestSiteBetList>()
                    {
                         bet
                    }
            };
            return JsonConvert.SerializeObject(param, Formatting.None);
        }

        private RequestSiteBetList setLotteryBetInfo(LotteryTypeEnum lotteryTypeEnum, string currentRoundCode, long betCount)
        {
            var result = new RequestSiteBetList();

            var isWin = false;
            if (betCount % 10 == 0)
                isWin = true;

            if (lotteryTypeEnum == LotteryTypeEnum.ChongqingSSC)
            {
                if (isWin)
                {
                    result.LotteryGameGroupId = 10;
                    result.LotteryGameModeId = 93;
                    result.BetNumber = "Dragon Tiger Flat";
                    result.BetMultiply = 1;
                    result.BetUnitStateKey = "dollar";
                    result.BetCount = 3;
                    result.BetAmount = 6;
                    result.BetUnit = 1;
                }
                else
                {
                    result.LotteryGameGroupId = 1;
                    result.LotteryGameModeId = 1;
                    result.BetNumber = "7|4|9|0|8";
                    result.BetMultiply = 1;
                    result.BetUnitStateKey = "dollar";
                    result.BetCount = 1;
                    result.BetAmount = 2;
                    result.BetUnit = 1;
                    result.RoundCode = "20200108034";
                }

            }
            else if (lotteryTypeEnum == LotteryTypeEnum.TianjinSSC)
            {
                if (isWin)
                {
                    result.LotteryGameGroupId = 43;
                    result.LotteryGameModeId = 365;
                    result.BetNumber = "Dragon Tiger Flat";
                    result.BetMultiply = 1;
                    result.BetUnitStateKey = "dollar";
                    result.BetCount = 3;
                    result.BetAmount = 6;
                    result.BetUnit = 1;
                }
                else
                {
                    result.BetAmount = 2;
                    result.BetCount = 1;
                    result.BetMultiply = 1;
                    result.BetNumber = "3|1|0|7|0";
                    result.BetUnit = 1;
                    result.BetUnitStateKey = "dollar";
                    result.LotteryGameGroupId = 34;
                    result.LotteryGameModeId = 300;
                }
            }
            else if (lotteryTypeEnum == LotteryTypeEnum.XinjiangSSC)
            {
                if (isWin)
                {
                    result.LotteryGameGroupId = 55;
                    result.LotteryGameModeId = 265;
                    result.BetNumber = "Dragon Tiger Flat";
                    result.BetMultiply = 1;
                    result.BetUnitStateKey = "dollar";
                    result.BetCount = 3;
                    result.BetAmount = 6;
                    result.BetUnit = 1;
                }
                else
                {
                    result.BetAmount = 2;
                    result.BetCount = 1;
                    result.BetMultiply = 1;
                    result.BetNumber = "2|5|4|0|5";
                    result.BetUnit = 1;
                    result.BetUnitStateKey = "dollar";
                    result.LotteryGameGroupId = 46;
                    result.LotteryGameModeId = 200;
                }
            }
            else if (lotteryTypeEnum == LotteryTypeEnum.BeijingPKTen)
            {
                if (isWin)
                {
                    result.LotteryGameGroupId = 19;
                    result.LotteryGameModeId = 81;
                    result.BetNumber = "Dragon Tiger";
                    result.BetMultiply = 1;
                    result.BetUnitStateKey = "dollar";
                    result.BetCount = 2;
                    result.BetAmount = 4;
                    result.BetUnit = 1;
                    result.RoundCode = "743254";
                }
                else
                {
                    result.BetAmount = 2;
                    result.BetCount = 1;
                    result.BetMultiply = 1;
                    result.BetNumber = "Small";
                    result.BetUnit = 1;
                    result.BetUnitStateKey = "dollar";
                    result.LotteryGameGroupId = 14;
                    result.LotteryGameModeId = 82;
                }

            }
            else if (lotteryTypeEnum == LotteryTypeEnum.BeijingHappyEight)
            {
                if (isWin)
                {
                    result.LotteryGameGroupId = 20;
                    result.LotteryGameModeId = 90;
                    result.BetNumber = "Upper Middle Lower";
                    result.BetMultiply = 1;
                    result.BetUnitStateKey = "dollar";
                    result.BetCount = 3;
                    result.BetAmount = 6;
                    result.BetUnit = 1;
                    result.RoundCode = "991766";
                }
                else
                {
                    result.BetAmount = 16;
                    result.BetCount = 8;
                    result.BetMultiply = 1;
                    result.BetNumber = "01 02 21 22|41 42 61 62";
                    result.BetUnit = 1;
                    result.BetUnitStateKey = "dollar";
                    result.LotteryGameGroupId = 21;
                    result.LotteryGameModeId = 89;
                }
            }
            result.RoundCode = currentRoundCode;
            return result;
        }

        public string getUrl()
        {
            return $"{ServerInfoSettingModel.Ip}/LotteryGame/AddManyBetsAsync";
        }
    }
}
