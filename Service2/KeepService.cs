﻿using Model2;
using Model2.LotteryProjectModel;
using Model2.LotteryProjectModel.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Service2
{
    public class KeepService
    {
        private static volatile KeepService instance;
        private static object initLocker = new Object();
        public MessageService MessageService = new MessageService();
        private int pollingSecond = 30;
        public static KeepService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (initLocker)
                    {
                        if (instance == null)
                        {
                            instance = new KeepService();
                        }
                    }
                }

                return instance;
            }
        }

        private KeepService() { }

        public void polling(UserModel userModel)
        {
            RequestHelper requestHelper = new RequestHelper();
            Task task = new Task(async () =>
            {
                while (true)
                {
                    try
                    {
                        var source = JsonConvert.SerializeObject("", Formatting.None);
                        var signature = new MD5Helper().Encrypt(Encoding.UTF8.GetBytes("" + ServerInfoSettingModel.MD5Hash));
                        var authorization = $"bearer {userModel.token}";

                        var response = await requestHelper.GetContentAsync(
                            HttpMethod.Post,
                            KeepSettingModel.Url,
                            "",
                            header: new System.Collections.Generic.Dictionary<string, string>() { { "Authorization", authorization }, { "signature", System.Net.WebUtility.UrlEncode(signature) } },
                            mediaType: "application/json"
                        );

                        var responseBody = await response.Content.ReadAsStringAsync();
                        if (!responseBody.Contains($"\"Code\":\"0\""))
                            MessageService.addMessage(MessageEnum.Warning, $"KeepApi {responseBody}");
                    }
                    catch (Exception ex)
                    {
                        MessageService.addMessage(MessageEnum.Error, $"啟用 KeepApi 失敗 ex:{ex.Message}");
                    }
                    Thread.Sleep(pollingSecond * 1000);
                }
            });
            task.Start();
        }
    }
}
