﻿using Model2;
using Model2.LotteryProjectModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Service2
{
    public class LoginService
    {
        private static volatile LoginService instance;
        private LoginSettingModel loginSettingModel = new LoginSettingModel();
        private CsvService csvService = new CsvService();
        private static object initLocker = new Object();
        public BlockingCollection<UserModel> Users = new BlockingCollection<UserModel>();
        public MessageService MessageService = new MessageService();
        public bool isStart { get; set; } = false;
        public static LoginService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (initLocker)
                    {
                        if (instance == null)
                        {
                            instance = new LoginService();
                        }
                    }
                }

                return instance;
            }
        }

        private LoginService() { }

        //public async Task loginAsync()
        //{
        //    var tasks = new List<Task>();

        //    var willLoginInfo = load();
        //    if (willLoginInfo.Rows.Count > 0)
        //    {
        //        isStart = true;
        //        int rowIndex = 0;
        //        foreach (DataRow dr in willLoginInfo.Rows)
        //        {
        //            string userAccount = dr[0].ToString();
        //            tasks.Add(LoginApiProcessAsync(userAccount));
        //            Thread.Sleep(1*100);
        //        }
        //    }
        //    Task.WaitAll(tasks.ToArray());
        //}

        public async Task loginAsync()
        {
            var willLoginInfo = load();
            if (willLoginInfo.Rows.Count > 0)
            {
                isStart = true;
                int rowIndex = 0;
                foreach (DataRow dr in willLoginInfo.Rows)
                {
                    string userAccount = dr[0].ToString();
                    await LoginApiProcessAsync(userAccount);
                    Thread.Sleep(1 * 100);
                }
            }
        }

        private DataTable load()
        {
            DataTable result = new DataTable();
            try
            {
                result = csvService.ReadCSV(LoginSettingModel.userFilePath);
            }
            catch (Exception ex)
            {
                MessageService.addMessage(MessageEnum.Error,$"讀取檔案失敗:{ex.ToString()}");
            }
            return result;
        }

        private async Task LoginApiProcessAsync(string userAccount)
        {
            var userModel = new UserModel()
            {
                account = userAccount
            };
            try
            {
                var param = new RequestUsersValidate()
                {
                    Account = userAccount,
                    Token = "xxx"
                };
                var source = JsonConvert.SerializeObject(param, Formatting.None);
                var signature = new MD5Helper().Encrypt(Encoding.UTF8.GetBytes(source + ServerInfoSettingModel.MD5Hash));

                var response = await new RequestHelperStatic().GetContentAsync(
                    HttpMethod.Post,
                    LoginSettingModel.loginUrl,
                    source,
                    header: new System.Collections.Generic.Dictionary<string, string>() { { "signature", signature } },
                    mediaType: "application/json"
                );

                var htmlHesult = await response.Content.ReadAsStringAsync();

                var jwtToken = JsonConvert.DeserializeObject<Result<JObject>>(htmlHesult).Data["access_token"].ToString();
                userModel.token = jwtToken;

                Users.Add(userModel);
                MessageService.addMessage(MessageEnum.Info, $"帳號:{param.Account} 登入成功");
            }
            catch (Exception ex)
            {
                MessageService.addMessage(MessageEnum.Error, $"登入失敗{ex.ToString()}");
            }
            KeepService.Instance.polling(userModel);
        }

    }
}
