﻿using Model2;
using Model2.LotteryProjectModel;
using Model2.LotteryProjectModel.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Service2
{
    public class LotteryRoundService
    {
        private static volatile LotteryRoundService instance;
        private LoginSettingModel loginModel = new LoginSettingModel();
        private CsvService csvService = new CsvService();
        private static object initLocker = new Object();
        public MessageService MessageService = new MessageService();
        public static LotteryRoundService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (initLocker)
                    {
                        if (instance == null)
                        {
                            instance = new LotteryRoundService();
                        }
                    }
                }

                return instance;
            }
        }

        List<LotteryRoundModel> lotteryRoundList = new List<LotteryRoundModel>();
        ConcurrentDictionary<LotteryTypeEnum, LotteryRoundModel> currentRoundDic = new ConcurrentDictionary<LotteryTypeEnum, LotteryRoundModel>();

        private LotteryRoundService() { }

        public void polling()
        {
            loadFile();

            Task task = new Task(() =>
            {
                while (true)
                {
                    try
                    {
                        foreach (LotteryTypeEnum lotteryTypeEnum in Enum.GetValues(typeof(LotteryTypeEnum)))
                        {
                            var dateTimeNow = DateTime.Now;

                            var update = true;
                            if (currentRoundDic.ContainsKey(lotteryTypeEnum))
                                update = (dateTimeNow >= currentRoundDic[lotteryTypeEnum].IssueDateTime && dateTimeNow < currentRoundDic[lotteryTypeEnum].SaleDeadline) != true;

                            if (update)
                            {
                                var newCurrentRound = lotteryRoundList.FirstOrDefault(r => dateTimeNow >= r.IssueDateTime && dateTimeNow < r.SaleDeadline && r.LotteryTypeId == lotteryTypeEnum);
                                if (newCurrentRound != null)
                                {
                                    currentRoundDic.AddOrUpdate(lotteryTypeEnum, newCurrentRound
                                    , (key, value) => key == lotteryTypeEnum ? newCurrentRound : value);
                                }
                            }
                        }
                        Thread.Sleep(1 * 1000);
                    }
                    catch (Exception ex)
                    {
                        MessageService.addMessage(MessageEnum.Error, $"更新獎期失敗 {ex.Message}");
                    }
                }
            });
            task.Start();
        }

        public LotteryRoundModel getCurrentRound(LotteryTypeEnum lotteryTypeEnum)
        {
            return currentRoundDic[lotteryTypeEnum];
        }

        private void loadFile()
        {
            try
            {
                var csvData = csvService.ReadCSV(LotteryRoundSettingModel.FilePath);
                if (csvData.Rows.Count == 0)
                {
                    MessageService.addMessage(MessageEnum.Error, $"讀取檔案失敗，筆數0");
                    return;
                }


                lotteryRoundList = csvData.AsEnumerable().Select(r => new LotteryRoundModel()
                {
                    LotteryTypeId = (LotteryTypeEnum)(int.Parse(r["LotteryTypeId"].ToString())),
                    RoundCode = r["RoundCode"].ToString(),
                    IssueDateTime = DateTime.Parse(r["IssueDateTime"].ToString()),
                    SaleDeadline = DateTime.Parse(r["SaleDeadline"].ToString()),
                }).ToList();
            }
            catch (Exception ex)
            {
                MessageService.addMessage(MessageEnum.Error,$"讀取檔案失敗:{ex.ToString()}");
            }
        }
    }
}
