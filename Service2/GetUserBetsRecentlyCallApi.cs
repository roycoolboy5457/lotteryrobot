﻿using Model2;
using Model2.LotteryProjectModel;
using Model2.LotteryProjectModel.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Service2.Interface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Service2
{
    public class GetUserBetsRecentlyCallApi : ICallApiStratege
    {
        public static PatternCallApi logic = new PatternCallApi();

        Stopwatch sw = new Stopwatch();
        UserModel userInfo;
        RequestHelper requestHelper;
        LotteryTypeEnum lotteryTypeEnum;
        MessageService messageService;
        public GetUserBetsRecentlyCallApi(UserModel userInfo, RequestHelper requestHelper, LotteryTypeEnum lotteryTypeEnum, MessageService messageService)
        {
            this.userInfo = userInfo;
            this.requestHelper = requestHelper;
            this.lotteryTypeEnum = lotteryTypeEnum;
            this.messageService = messageService;
        }

        public async Task CallApiAsync() 
        {
            try
            {
                var source = createSource();
                var signature = logic.createSignature(source);
                var authorization = $"bearer {userInfo.token}";

                sw.Reset();
                sw.Start();
                var response = await callApiAsync(source, authorization, signature);
                var responseBody = await response.Content.ReadAsStringAsync();
                sw.Stop();

                recodeMessage(responseBody);
            }
            catch (Exception ex)
            {
                messageService.addMessage(MessageEnum.Error, $"{ex.ToString()}");
                logic.ErrorCount++;
            }
        }

        protected void recodeMessage(string responseBody)
        {
            if (!responseBody.Contains($"\"Code\":\"0\"") && !responseBody.Contains($"\"Code\":\"10033\""))
            {
                messageService.addMessage(MessageEnum.Warning, $"bet {responseBody.ToString()}");
                logic.WarningCount++;
            }
            else
            {
                logic.SucessCount++;
                logic.TotalSpanElapsedTime += sw.ElapsedMilliseconds;
                logic.AvgSucessSpanTime = logic.TotalSpanElapsedTime / logic.SucessCount;
            }
        }

        protected async Task<HttpResponseMessage> callApiAsync(string source, string authorization, string signature)
        {
            return await requestHelper.GetContentAsync(
                   HttpMethod.Post,
                   getUrl(),
                   source,
                   header: new System.Collections.Generic.Dictionary<string, string>() { { "Authorization", authorization }, { "signature", System.Net.WebUtility.UrlEncode(signature) } },
                   mediaType: "application/json"
               );
        }

        protected string createSource()
        {
            var param = new RequestLotteryGameGetUserBetsRecently()
            {
                LotteryTypeId = lotteryTypeEnum
            };

            return JsonConvert.SerializeObject(param, Formatting.None);
        }

        public string getUrl()
        {
            return $"{ServerInfoSettingModel.Ip}/LotteryGame/GetUserBetsRecently";
        }
    }
}
