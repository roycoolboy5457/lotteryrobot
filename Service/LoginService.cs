﻿using Model;
using Model.LotteryProjectModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Service
{
    public class LoginService
    {
        private static volatile LoginService instance;
        private LoginModel loginModel = new LoginModel();
        private CsvService csvService = new CsvService();
        private static object initLocker = new Object();

        public static LoginService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (initLocker)
                    {
                        if (instance == null)
                        {
                            instance = new LoginService();
                        }
                    }
                }

                return instance;
            }
        }

        private LoginService() { }

        public void login()
        {
            DataTable willLoginInfo = load();
            foreach (DataRow dr in willLoginInfo.Rows)
            {
                string userAccount = dr[0].ToString();
            }
        }

        private (string result,DataTable dt) load()
        {
            try
            {
                return ("", csvService.ReadCSV(loginModel.userFilePath));
            }
            catch (Exception ex)
            {
                return (ex.ToString(), new DataTable());
            }

        }

        private string loginApiProcess(string userAccount)
        {
            var param = new RequestUsersValidate()
            {
                Account = userAccount,
                Token = "xxx"
            };
            var source = JsonConvert.SerializeObject(param, Formatting.None);
            var signature = new MD5Helper().Encrypt(Encoding.UTF8.GetBytes(source + Config.MD5Hash));
            Console.WriteLine("url => " + loginUrl);
            Console.WriteLine("body => " + source);
            Console.WriteLine("signature => " + signature);

            var response = await new RequestHelper().GetContentAsync(
                HttpMethod.Post,
                loginUrl,
                source,
                header: new System.Collections.Generic.Dictionary<string, string>() { { "signature", signature } },
                mediaType: "application/json"
            );

            var htmlHesult = await response.Content.ReadAsStringAsync();

            var jwtToken = JsonConvert.DeserializeObject<Result<JObject>>(htmlHesult).Data["access_token"].ToString();

            Users.Add(new UserInfo()
            {
                account = param.Account,
                token = jwtToken
            });

        }
    }
}
